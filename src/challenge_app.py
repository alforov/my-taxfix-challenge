import json    
from utils.schema_validator import SchemaValidator
from utils.events_reporter import Reporter

sV = SchemaValidator() #instaniating the validator
events_dict = Reporter() #instantiating the reporter

print("--------SCHEMA VALIDATION START--------")
#Reading line by line from json file
#Validate each line and create the reporter of events (based on dictionary)
with open("input.json", "r+") as file:
    for line in file:
        jsonschema = json.loads(line)
        sV.schemaValidation(jsonschema)
        events_dict.buildDictionary(jsonschema, "timestamp","event") #build dictionary with events per date

print("--------SCHEMA VALIDATION END--------\n")

print("============PRINT EVENTS REPORT=========")
events_dict.printReport()