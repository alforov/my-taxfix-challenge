import json    
import jsonschema    
import logging
from jsonschema import Draft7Validator
from datetime import datetime

# TASK 1.2: Implement a schema validator
class SchemaValidator:
    def __init__(self):
        # TASK 1.1: Define a schema based on input.json file attached
        self.schema = {
            "type" : "object",
            "properties" : {
                "id" : {"type" : "string"},
                "received_at" : {"type" : "string"},
                "anonymous_id" : {"type" : "string"},
                "context_app_version" : {"type" : "string"},
                "context_device_ad_tracking_enabled" : {"type" : "boolean"},
                "context_device_manufacturer" : {"type" : "string"},
                "context_device_model" : {"type" : "string"},
                "context_device_type" : {"type" : "string"},
                "context_library_name" : {"type" : "string"},
                "context_library_version" : {"type" : "string"},
                "context_locale" : {"type" : "string"},
                "context_network_wifi" : {"type" : "boolean"},
                "context_os_name" : {"type" : "string"},
                "context_timezone" : {"type" : "string"},
                "event" : {"type" : "string"},
                "event_text" : {"type" : "string"},
                "original_timestamp" : {"type" : "string"},
                "sent_at" : {"type" : "string"},
                "timestamp" : {"type" : "string"},
                "user_id" : {"type" : "string"},
                "context_network_carrier" : {"type" : "string"},
                "context_device_token" : {"type" : "null"},
                "context_traits_taxfix_language" : {"type" : "string"},
            },
            "additionalProperties": False,
            "required": [
                        "id","received_at","anonymous_id","context_app_version",
                        "context_device_ad_tracking_enabled","context_device_manufacturer",
                        "context_device_model","context_device_type","context_library_name",
                        "context_library_version","context_locale","context_network_wifi",
                        "context_os_name","context_timezone","event","event_text","original_timestamp",
                        "sent_at","timestamp","user_id","context_network_carrier","context_device_token",
                        "context_traits_taxfix_language"
                        ]
        }

    def schemaValidation (self, jsonschema):
        print("--------Validation for the instance with ID: [",jsonschema["id"],"]--------")
        v = Draft7Validator(self.schema)
        errors = sorted(v.iter_errors(jsonschema), key=lambda e: e.path)
        # TASK 1.3: In case of errors (wrong data type, missing field, etc.), the validator should log them and continue to next line of data
        if len(errors):
            for error in errors:
                print("\nERROR: ", error.message) #logging the errors
        else:
            print("Schema is OK")