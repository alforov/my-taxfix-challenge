import json    
from datetime import datetime

#Objective 2: Generate a report with count of each “event” in the file
class Reporter:
    def __init__(self):
        self.events_dict =dict()

    def buildDictionary (self, js_schema, key, value): #key is "timestamp" and value is "event" from json schema
        #Partitioned by timestamps, we can find counts of each event per date for the reporting

        dt = str(datetime.strptime(js_schema[key], '%Y-%m-%d %H:%M:%S.%f').date()) 
        #extract date from timestamp and use as a key in dictionary search
            
        if dt in self.events_dict.keys():# if key is present in the list, just append the value (event and count)
            if js_schema[value] in self.events_dict[dt].keys():  #find event for a date and update its counter
                self.events_dict[dt]={js_schema[value],self.events_dict[dt:js_schema[value]] + 1}
                #if the same event occured on the same date, we increase the counter
            else:
                self.events_dict[dt] = {js_schema[value] : 1}
        else:
            self.events_dict[dt]={js_schema[value] : 1}
        
        return self.events_dict

    def printReport (self):
        #Print out the information about events per date
        for date, event_info in sorted(self.events_dict.items()):
            # If value is dict type, then print nested dict (event : counter)
            print("\nDATE:", date)
            for event in event_info:
                print(event,':', event_info[event])