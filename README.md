# My Taxfix challenge

This repository contains an application that reads line by line from JSON file and validates each row on correctness. Every error is logged out with statement about error.
In addition, application prints the report about events that happened on every date with number of these events.

## Validation
Challenge application uses a class called `SchemaValidator`. There is a defined schema taken as template to compare the json schemas from file. Class uses `Draft7Validator` from `jsonschema` library that has a standardised errors categories and corresponding messages.

## Reporting
Challenge application also uses a class called `Reporter`. It build a dictionary with the following structure:
events_dictionary

 └── date

    ├── event_name

    └── counter (number of events)

The instance of the class provides a possibility to print out the information about events per date.

## How to run and test
The solution is Containerize by using Docker. To build the docker image, go the the root of the project and run following command in the terminal:
    ```docker build -t mychallenge_image .``` 
After the build is successfully finished, run the following command to run the image as a container and see the logs output:
    ```docker run -it --rm mychallenge_image```